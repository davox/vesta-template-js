clone project
* move node-v1.sh node-v1.stpl node-v1.tpl in this path: "/usr/local/vesta/data/templates/web/nginx"
* move app-nginx.conf.example to /home/your-user/web/your-domain/
* rename from app-nginx.conf.example to app-nginx.conf
* change permission file with chown your-user:your-user ./app-nginx.conf
* open file app-nginx.conf and change port
* open vesta panel go to editing domain
and under proxy support select node-v1 from dropdown form
