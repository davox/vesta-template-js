#!/bin/bash

user=$1
domain=$2
ip=$3
home=$4
docroot=$5
fileconf="$home/$user/web/$domain/app-nginx.conf"
if [ ! -f "$fileconf" ]; then
	touch "$home/$user/web/$domain/app-nginx.conf"
	runuser -l $user -c "echo proxy_pass      http://127.0.0.1:3333\;" >> "$home/$user/web/$domain/app-nginx.conf"
	chown $user:$user "$home/$user/web/$domain/app-nginx.conf"
fi

